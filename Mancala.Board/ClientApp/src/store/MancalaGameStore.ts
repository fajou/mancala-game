import {Action, Reducer} from 'redux';
import {AppThunkAction} from './';

export interface Hole {
    index: number;
    pieces: number;
    isMancala: boolean;
    side: number;
}

export interface GameState {
    isLoading: boolean;
    startIndex?: number;
    game: Game;
}

export interface Board {
    holes: Hole[];
}

export interface Game {
    board: Board;
    ending: boolean;
    turn: Turn;
    winner: Hole;
}

export interface Turn {
    currHole?: Hole;
    player: number;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestMancalaGameAction {
    type: 'REQUEST_MANCALA_GAME';
    startIndex: number;
}

interface ReceiveMancalaGameAction {
    type: 'RECEIVE_MANCALA_GAME';
    startIndex: number;
    game: Game;
}

type KnownAction = RequestMancalaGameAction | ReceiveMancalaGameAction;

export const actionCreators = {
    requestMancalaGame: (startIndex: number, index?: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.mancalaGame && startIndex !== appState.mancalaGame.startIndex) {
            const input = index !== undefined ? `api/mancala/${index}` : `api/mancala`;
            fetch(input)
                .then(response => response.json() as Promise<Game>)
                .then(data => {
                    dispatch({type: 'RECEIVE_MANCALA_GAME', startIndex: startIndex, game: data});
                });

            dispatch({type: 'REQUEST_MANCALA_GAME', startIndex: startIndex});
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: GameState = {
    game: {
        board: {
            holes: []
        },
        turn: {
            currHole: undefined,
            player: 0
        },
        ending: false
    },
    isLoading: false
};

export const reducer: Reducer<GameState> = (state: GameState | undefined, incomingAction: Action): GameState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_MANCALA_GAME':
            return {
                startIndex: action.startIndex,
                game: state.game,
                isLoading: true
            };
        case 'RECEIVE_MANCALA_GAME':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startIndex === state.startIndex) {
                return {
                    startIndex: action.startIndex,
                    game: action.game,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};

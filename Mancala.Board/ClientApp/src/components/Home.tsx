import * as React from 'react';
import {connect} from 'react-redux';
import './Styles/Home.css';
import * as MancalaGameStore from "../store/MancalaGameStore";
import {ApplicationState} from "../store";
import {Hole} from "../store/MancalaGameStore";

type MancalaGameProps =
    MancalaGameStore.GameState
    & typeof MancalaGameStore.actionCreators;

class Home extends React.PureComponent<MancalaGameProps> {

    private fetchInitData() {
        this.props.requestMancalaGame(0);
    }

    private fetchGameMoveFrome(hole: Hole) {
        if (this.props.game.turn.player !== hole.side)
            return;

        const startIndex = this.props.startIndex ? this.props.startIndex : 1;
        this.props.requestMancalaGame(startIndex + 1, hole.index)
    }

    public componentDidMount() {
        this.fetchInitData();
    }

    public renderMancalaPlayer() {
        return (
            <>
                {this.props.game.board.holes.map((hole, index) => {
                    if (hole.isMancala && hole.side === 1)
                        return <div key={index} id="mancala2" className={`mancala ${index}`}>{hole.pieces}</div>
                    if (hole.isMancala && hole.side === 0)
                        return <div key={index} id="mancala1" className={`mancala ${index}`}>{hole.pieces}</div>
                    return undefined;
                })}
            </>
        );
    }

    public renderHolePlayer(player: number) {
        let sides = this.props.game.board.holes.filter(hole => hole.side === player && !hole.isMancala);
        sides = player == 1 ? sides.sort((a, b) => a.index < b.index ? 1 : -1) : sides;
        return (
            <div className="rows">
                {sides.map((hole, index) => {
                        const locked = player !== this.props.game.turn.player || hole.pieces === 0 ? "lock" : "";
                        return (
                            <div key={index} id={`${hole.index}`} onClick={() => this.fetchGameMoveFrome(hole)}
                                 className={`pod player${hole.side + 1} ${locked}`}>
                                {hole.pieces}
                            </div>
                        );
                    }
                )}
            </div>
        );
    }

    public renderPlayer(player: number) {
        const id = player === 0 ? "player-one" : "player-two";
        const curr = this.props.game.turn.player === player ? "player currentPlayer" : "player";
       
        return (
            <div id={id} className={curr}>
                Player {player + 1}
            </div>
        );
    }

    public renderWinner() {
        if (this.props.game.ending) {
            return (
                <>
                    <h2 className="player-win">The winner is the player {this.props.game.winner.side + 1} with {this.props.game.winner.pieces} points</h2>
                    <hr />
                </>
            );
        }

        return undefined;
    }

    public render() {
        return (
            <>
                {this.renderWinner()}
                {this.renderPlayer(1)}
                <div>
                    <div id="board" className="board">
                        {this.renderMancalaPlayer()}
                        {this.renderHolePlayer(1)}
                        {this.renderHolePlayer(0)}
                    </div>
                </div>
                {this.renderPlayer(0)}
                <hr/>
                <div className="restart-div">
                    <button onClick={() => this.fetchInitData()} id="restart">Restart</button>
                </div>
            </>
        );
    }
}

export default connect((state: ApplicationState) => state.mancalaGame, MancalaGameStore.actionCreators)(Home as any);

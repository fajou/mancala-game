using Mancala.Engine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Mancala.Board.Controllers
{
    [ApiController]
    [Route("api/mancala")]
    public class MancalaController : ControllerBase
    {
        private readonly ILogger<MancalaController> _logger;
        private static Game MancalaGame { get; set; }

        public MancalaController(ILogger<MancalaController> logger)
        {
            _logger = logger;
        }

        private static void InitGame()
        {
            MancalaGame = new Game();
            MancalaGame.Board.Init(4);
        }

        [HttpGet]
        [Route("")]
        public Game Start()
        {
            InitGame();
            return MancalaGame;
        }

        [HttpGet]
        [Route("{id:int}")]
        public Game MoveFrom(int? id)
        {
            var index = id ?? -1;
            if (index is -1)
                return MancalaGame;
            
            MancalaGame.MoveFrom(index);
            return MancalaGame;
        }
    }
}
using System;
using Mancala.Engine;
using NUnit.Framework;

namespace Mancala.Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        public static OnMove[] InitGameEnding()
        {
            var holes1 = new Hole[14];
            var holes2 = new Hole[14];
            for (var i = 0; i < holes1.Length; i++)
            {
                if (i < 7)
                    holes1[i] = new Hole
                    {
                        Side = Player.P1,
                        Index = i,
                        IsMancala = i == 6,
                        Pieces = 0
                    };
                else
                {
                    holes1[i] = new Hole
                    {
                        Side = Player.P2,
                        Index = i,
                        IsMancala = i == 13,
                        Pieces = 5
                    };
                }
            }

            holes1[6].Pieces = 10;

            for (var i = 0; i < holes2.Length; i++)
            {
                if (i < 7)
                    holes2[i] = new Hole
                    {
                        Side = Player.P1,
                        Index = i,
                        IsMancala = i == 6,
                        Pieces = 6
                    };
                else
                {
                    holes2[i] = new Hole
                    {
                        Side = Player.P2,
                        Index = i,
                        IsMancala = i == 13,
                        Pieces = 0
                    };
                }
            }

            holes2[^1].Pieces = 16;

            var board1 = new OnMove(holes1);
            var board2 = new OnMove(holes2);

            return new[] {board1, board2};
        }

        [Test]
        public void TestGameEnding()
        {
            var boards = InitGameEnding();
            var game1 = new Game(boards[0]);
            var game2 = new Game(boards[1]);

            Assert.True(game1.IsGameEnding());
            Assert.True(game2.IsGameEnding());
        }

        [Test]
        public void TestInitBoard()
        {
            var board = new OnMove();
            board.Init(4);

            for (var i = 0; i < board.Holes.Length - 1; i++)
            {
                var hole = board.Holes[i];
                if (hole.Index < 6)
                    Assert.True(hole.Pieces == 4 && hole.Side == Player.P1);
                else if (6 < hole.Index)
                    Assert.True(hole.Pieces == 4 && hole.Side == Player.P2);
            }

            Assert.True(board.Holes[6].Pieces == 0 && board.Holes[6].IsMancala && board.Holes[6].Side == Player.P1);
            Assert.True(board.Holes[^1].Pieces == 0 && board.Holes[^1].IsMancala && board.Holes[^1].Side == Player.P2);
        }

        [Test]
        public void TestMoveFrom()
        {
            var game = new Game();
            game.Board.Init(4);
            game.MoveFrom(2);
            game.MoveFrom(3);

            game.MoveFrom(8); // index 8 is on range 2 pits
            game.MoveFrom(9); // index 9 is on range 3 pits
            foreach (var hole in game.Board.Holes)
            {
                Console.WriteLine($"{hole.Pieces} {hole.Side} - {hole.Index}");
            }

            Console.WriteLine(game.Turn.Player);
        }

        [Test]
        public void TestEmptySide()
        {
            var game = new Game();
            game.Board.Init(4);
            game.Board.Holes[2].Pieces = 1;
            game.Board.Holes[3].Pieces = 0; // Empty On My Side

            var opposite = game.Board.Opposite(3);
            game.MoveFrom(2);

            Assert.Zero(opposite.Pieces);
            Assert.AreEqual(game.Board.Get(2).Pieces, 0);
            Assert.AreEqual(game.Board.Get(3).Pieces, 0);
            Assert.AreEqual(game.Board.GetMancala(Player.P1).Pieces, 5);
        }
    }
}
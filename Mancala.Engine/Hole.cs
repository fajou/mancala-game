namespace Mancala.Engine
{
    public class Hole : Mancala
    {
        public void Remove()
        {
            Pieces = 0;
        }
    }
}
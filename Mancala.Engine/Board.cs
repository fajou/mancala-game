using System.Linq;

namespace Mancala.Engine
{
    public class Board
    {
        public Hole[] Holes { get; }

        public Board(Hole[] holes)
        {
            Holes = holes ?? new Hole[14];
        }

        public void Init(int capacity)
        {
            var index = 0;
            var center = Holes.Length / 2;
            while (index < Holes.Length)
            {
                Holes[index] = new Hole
                {
                    Index = index,
                    Side = index < center ? Player.P1 : Player.P2,
                    Pieces = capacity
                };
                index++;
            }

            Holes[center - 1].IsMancala = true;
            Holes[center - 1].Pieces = 0;

            Holes[^1].IsMancala = true;
            Holes[^1].Pieces = 0;
        }

        public Mancala GetMancala(Player player)
        {
            return Holes.FirstOrDefault(hole => hole.IsMancala && hole.Side == player);
        }
    }
}
namespace Mancala.Engine
{
    public class Game
    {
        public OnMove Board { get; }
        public Turn Turn { get; set; }
        public Mancala Winner { get; set; }
        public bool Ending { get; private set; }

        public Game(OnMove move = null, Turn turn = null)
        {
            Turn = turn ?? new Turn {Player = Player.P1};
            Board = move ?? new OnMove();
        }

        private Mancala GetWinner()
        {
            var m1 = Board.GetMancala(Player.P1);
            var m2 = Board.GetMancala(Player.P2);

            return m1.Pieces < m2.Pieces ? m2 : m1;
        }

        public void MoveFrom(int index)
        {
            var startHole = Board.Get(index);
            if (startHole.IsMancala || startHole.Pieces == 0 || Ending)
                return;

            var pieces = startHole.Pieces;
            startHole.Remove();

            var hole = Put(startHole, ref pieces);
            if (!hole.IsMancala)
            {
                if (hole.Pieces == 1 && Turn.Player == hole.Side)
                {
                    var opposite = Board.Opposite(hole.Index);
                    if (opposite.Pieces > 0)
                    {
                        var mancala = Board.GetMancala(Turn.Player);
                        mancala.Pieces += opposite.Pieces + hole.Pieces;
                        opposite.Remove();
                        hole.Remove();
                    }
                }

                Turn.Player = Turn.Player == Player.P1 ? Player.P2 : Player.P1;
            }

            Turn.CurrHole = hole;
            Ending = IsGameEnding();
            Turn.Player = Ending ? Player.End : Turn.Player;
            Winner = Ending ? GetWinner() : new Mancala();
        }

        public Hole Put(Hole start, ref int pieces)
        {
            while (true)
            {
                var hole = Board.Next(start.Index);
                if (hole.IsMancala && hole.Side != Turn.Player)
                    return Board.Next(hole.Index);

                pieces--;
                hole.PutOne();

                if (pieces == 0)
                    return hole;
                start = hole;
            }
        }

        public bool IsGameEnding()
        {
            var index = 0;
            var center = Board.Holes.Length / 2;
            var empty = true;

            while (index < Board.Holes.Length - 1)
            {
                var hole = Board.Get(index);
                if (hole.Pieces != 0 && index < center - 1)
                {
                    index = center;
                    empty = false;
                    continue;
                }

                if (empty && center <= index)
                    return true;

                if (hole.Pieces != 0 && center <= index)
                    return false;
                index++;
            }

            return true;
        }
    }
}
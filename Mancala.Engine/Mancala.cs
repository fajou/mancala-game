namespace Mancala.Engine
{
    public class Mancala
    {
        public int Index { get; set; }
        public int Pieces { get; set; }
        public bool IsMancala { get; set; }
        public Player Side { get; set; }

        public void PutOne()
        {
            Pieces++;
        }
    }
}
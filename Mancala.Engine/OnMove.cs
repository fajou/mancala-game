namespace Mancala.Engine
{
    public class OnMove : Board
    {
        public Hole Next(int currIndex)
        {
            return currIndex < Holes.Length - 1 ? Holes[currIndex + 1] : Holes[0];
        }

        public Hole Get(int index)
        {
            return Holes[index];
        }

        public Hole Opposite(int index)
        {
            var idx = Holes.Length - 2 - index;
            return Holes[idx];
        }

        public OnMove(Hole[] holes = null) : base(holes)
        {
        }
    }
}
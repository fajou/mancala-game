namespace Mancala.Engine
{
    public class Turn
    {
        public Hole CurrHole { get; set; }
        public Player Player { get; set; }
    }
}